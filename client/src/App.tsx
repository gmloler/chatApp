import { useContext } from "react";
import { ThemeContext } from "./context/ThemeContext";

function App() {
  const { theme, setTheme } = useContext(ThemeContext);

  return (
    <div className="App">
      <div className={theme}>
        <div className="background full-container flex column align-center justify-center">
          <h1 className="text">Hello Vite + React!</h1>
          <button
            className="button"
            onClick={() => {
              setTheme(theme === "light" ? "dark" : "light");
            }}
          >
            Switch Theme {theme}
          </button>
        </div>
      </div>
    </div>
  );
}

export default App;
